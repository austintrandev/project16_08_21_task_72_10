package com.devcamp.relationship.model;

import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name="users")
public class CUser {
	public CUser(String userName, String firstName, String lastName, Set<CPost> posts) {

		this.userName = userName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.posts = posts;
	}

	public CUser() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "username")
	private String userName;
	
	@Column(name = "fistname")
	private String firstName;
	
	@Column(name = "lastname")
	private String lastName;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "createdBy")
	private Set<CPost> posts;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Set<CPost> getPosts() {
		return posts;
	}

	public void setPosts(Set<CPost> posts) {
		this.posts = posts;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

}
