package com.devcamp.relationship.model;

import javax.persistence.*;

@Entity
@Table(name="posts")
public class CPost {
	public CPost() {
		super();
	}

	public CPost(String title, String content) {
		super();
		this.title = title;
		this.content = content;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "title")
	private String title;
	
	@Column(name = "content")
	private String content;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="created_by", nullable = false)
	private CUser createdBy;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTittle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public CUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(CUser createdBy) {
		this.createdBy = createdBy;
	}

}
